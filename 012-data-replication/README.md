# 012-data-replication

L'objectif de ce TP est définir un objet de type SyncSet pour mettre en cache les données de certains objets et de réaliser un couple constraint/constrainttemplate en accédant aux données répliquées.

Synchroniser tous les namespaces, pods et tous les services du cluster : 

```yaml
apiVersion: syncset.gatekeeper.sh/v1alpha1
kind: SyncSet
metadata:
  name: syncset
spec:
  gvks:
    - group: ""
      version: "v1"
      kind: <TODO>
    - group: ""
      version: "v1"
      kind: <TODO>
    - group: ""
      version: "v1"
      kind: <TODO>
```

Réaliser ensuite une ConstraintTemplate qui n'autorise qu'un seul service de type `NodePort` avec un label spécifique dans tout le cluster.
Choisissez le label spécifique et implémenter le dans la Constraint associée.

Tenter ensuite de créer les services en question

Quelques ressources utiles : 

- la commande pour créer un service de type nodeport est `kubectl create service nodeport myservice -n <namespace> --tcp=80:80`

- les données synchronisées sont accessible depuis `data.inventory.cluster[<groupVersion>][<kind>]|<name>]` pour les objets de type ClusterScoped et depuis `data.inventory.namespace[<namespace>][groupVersion][<kind>][<name>]` pour les objets namespaced scope.

le yaml du constrainttemplate pré-configuré :

```yaml
apiVersion: templates.gatekeeper.sh/v1
kind: ConstraintTemplate
metadata:
  name: k8sallowserviceifpod
  annotations:
    metadata.gatekeeper.sh/title: "Allow Service If Pod"
    metadata.gatekeeper.sh/version: 1.0.0
    description: >-
      Allow a service to be created if a pod exists in the namespace
spec:
  crd:
    spec:
      names:
        kind: K8sAllowServiceIfPod
  targets:
    - target: admission.k8s.gatekeeper.sh
      rego: |
       TODO
```

## Bonus

Créer un autre constraint template qui autorise la création de service si il y a au moins 1 pod dans le même namespace avec un label.

Déclinez avec une constraint et tester.