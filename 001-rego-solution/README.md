# Solution 001-rego

## FOR SOME

`opa eval --input 001-rego/input.json -d file:///$(pwd)/001-rego-solution/for-some.rego 'data.rego.shell_accessible'`

```
{
  "result": [
    {
      "expressions": [
        {
          "value": [
            "app",
            "busybox"
          ],
          "text": "data.rego.shell_accessible",
          "location": {
            "row": 1,
            "col": 1
          }
        }
      ]
    }
  ]
}
```

## EVERY

`opa eval --input 001-rego/input.json -d file:///$(pwd)/001-rego-solution/every.rego 'data.rego'`

```
{
  "result": [
    {
      "expressions": [
        {
          "value": {
            "any_telnet_exposed": true
          },
          "text": "data.rego",
          "location": {
            "row": 1,
            "col": 1
          }
        }
      ]
    }
  ]
}
```

`opa eval --input 001-rego-solution/input-no-telnet.json -d file:///$(pwd)/001-rego-solution/every.rego 'data.rego'`

```
{
  "result": [
    {
      "expressions": [
        {
          "value": {
            "no_telnet_exposed": true
          },
          "text": "data.rego",
          "location": {
            "row": 1,
            "col": 1
          }
        }
      ]
    }
  ]
}
```

## Policy

`opa eval --input 001-rego/input.json -d 001-rego-solution/policy.rego 'some x ; data.rego.violation[x]'`