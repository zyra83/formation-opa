package rego

import rego.v1

no_telnet_exposed if {
    every server in input.servers {
        every protocol in server.protocols {
            "telnet" != protocol
        }
    }
}

any_telnet_exposed if {
    some server in input.servers
    "telnet" in server.protocols
}
