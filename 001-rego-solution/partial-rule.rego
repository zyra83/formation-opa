package rego

import rego.v1

public_network contains net.id if {
    some net in input.networks # some network exists and..
    net.public                 # it is public.
}