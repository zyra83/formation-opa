# 004-builtin-solution

## les nombres

```
> my_number := 1.25
Rule 'my_number' defined in package repl. Type 'show' to see rules.
> ceil(my_number)
2
```

```
> my_number := -10
Rule 'my_number' re-defined in package repl. Type 'show' to see rules.
> abs(my_number)
10
```

```
> my_array := numbers.range(5,9)
Rule 'my_array' defined in package repl. Type 'show' to see rules.
> my_array
[
  5,
  6,
  7,
  8,
  9
]
```

## les types

```
> my_str := "hello, world"
Rule 'my_str' defined in package repl. Type 'show' to see rules.
> type_name(my_str)
"string"
> is_string(my_str)
true
> is_number(my_str)
false
> is_null(my_str)
false
> my_number := "123"
Rule 'my_number' re-defined in package repl. Type 'show' to see rules.
> type_name(my_number)
"string"
> to_number(my_number)
123
```

## les agrégations

```
> my_set = {10,6,9,54,6,7}
Rule 'my_set' defined in package repl. Type 'show' to see rules.
> count(my_set)
5
> min(my_set)
6
> max(my_set)
54
> sum(my_set)
86
```

## les valeurs composites

```
> arr1 := ["hello", "world"]
Rule 'arr1' re-defined in package repl. Type 'show' to see rules.
> arr2 := ["goodbye", "everyone"]
Rule 'arr2' defined in package repl. Type 'show' to see rules.
> array.concat(arr1, arr2)
[
  "hello",
  "world",
  "goodbye",
  "everyone"
]
> arr_concat := array.concat(arr1, arr2)
Rule 'arr_concat' defined in package repl. Type 'show' to see rules.
> array.slice(arr_concat, count(arr_concat) - 2, count(arr_concat))
[
  "goodbye",
  "everyone"
]
```

```
> user
{
  "user": {
    "active": true,
    "email": "john.doe@example.com",
    "first_name": "John",
    "id": 1,
    "last_name": "Doe",
    "preferences": {
      "language": "en",
      "theme": "dark"
    },
    "role": "user",
    "username": "john_doe"
  }
}
> json.filter(user, ["user/preferences/language"])
{
  "user": {
    "preferences": {
      "language": "en"
    }
  }
}
> json.remove(user, ["user/email"])
{
  "user": {
    "active": true,
    "first_name": "John",
    "id": 1,
    "last_name": "Doe",
    "preferences": {
      "language": "en",
      "theme": "dark"
    },
    "role": "user",
    "username": "john_doe"
  }
}
```

## les string

```
package example

allow_access {
    input.user.full_name == "John Doe"
}
```

```
package example

allow_access {
    count(input.user.username) >= 5
}
```
```
package example

allow_access {
    startswith(input.user.email, "admin@")
}
```

```
package example

allow_access {
    not contains(input.user.role, "guest")
}
```

```
package example

allow_access {
    input.user.role == "ADMIN"
}
```

```
package example

allow_access {
    startswith(input.user.username, "JD")
}
```

```
package example

allow_access {
    not contains(input.user.username, "blocked")
}
```

## les regexp

```
package example

allow_access {
    re.match("^\\S+@\\S+\\.\\S+$", input.user.email)
}
```

```
package example

allow_access {
    re.match("^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d).{8,}$", input.user.password)
}
```

```
package example

allow_access {
    re.search("/api/", input.resource.url)
}
```

```
package example

allow_access {
    re.match(".*\\.txt$", input.resource.filename)
}
<>
```
package example

allow_access {
    re.match("^\\+\\d+$", input.user.phone_number)
}
```

```
package example

allow_access {
    re.match("^[a-z0-9]+$", input.user.identifier)
}
```

## jwt

> jwt := "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.7iMCOyT_C2dXdorMdC-8mYDRjXpRBQcIh9P8ldzK8iU"
Rule 'jwt' defined in package repl. Type 'show' to see rules.
> io.jwt.decode(jwt)
[
  {
    "alg": "HS256",
    "typ": "JWT"
  },
  {
    "iat": 1516239022,
    "name": "John Doe",
    "sub": "1234567890"
  },
  "ee23023b24ff0b6757768acc742fbc9980d18d7a5105070887d3fc95dccaf225"
]
