# 010 constraint

Appliquer le constrainttemplate :

`kubectl apply -f k8srequiredlabels_template.yaml`

Créer une constraint qui se base sur le template ci-dessus et qui nécessite le label `gatekeeper` sur tous les `namespaces` du cluster

```
apiVersion: constraints.gatekeeper.sh/v1beta1
kind: K8sRequiredLabels
metadata:
  name: <TODO>
spec:
  match:
    kinds:
      - apiGroups: [<TODO>]
        kinds: [<TODO>]
  parameters:
    labels: [<TODO>]
```

Tenter maintenant de créer un namespace et observer le résultat : 

`kubectl create ns foobar`

Essayer de recreer le namespace avec le bon label : 

```
kubectl apply -f - <<EOF
apiVersion: v1
kind: Namespace
metadata:
  name: foobar
  <TODO>
EOF
```

Supprimez la constraint et enlever le label au namespace :

`kubectl delete constraint/<TODO>`
`kubectl label ns/foobar gatekeeper-`

Remettez la constrainte en place et lancer un describe dessus une 30aine de secondes plus tard.
Dans le describe, vous pouvez voir que le namespace `foobar` (sans labels) viole cette constrainte

Supprimez le namespace

Passez l'enforcementAction à `dryrun` et récréer le namespace sans labels. Analyser le résultat.

Cleanup

```
kubectl delete k8srequiredlabels.constraints.gatekeeper.sh ns-must-have-gk
kubectl delete constrainttemplate k8srequiredlabels
```
## Bonus

Inspirez-vous de la constrainttemplate `k8srequiredlabels` et réalisez une constraint template qui nécessite une annotation.

Créer ensuite une constraint qui nécessite l'annotation "foobar"

Ne pas oublier de cleanup ses ressources à la fin du TP.