# 016-expansion-template

Appliquer la ConstraintTemplate `k8srequiredlabels` : `kubectl apply -f k8srequiredlabels_template.yaml`

Créer une constraint qui utilise ce ConstraintTemplate et qui demande à ce que les pods aient le label `gatekeeper`.

Tenter un déploiement : 

`kubectl apply -f nginx.yaml`

Observer le nombre de pods présent.

Supprimer le deploiement

`kubectl delete deployment nginx`

Créer ensuite un ExpansionTemplate : 

```yaml
apiVersion: expansion.gatekeeper.sh/v1alpha1
kind: ExpansionTemplate
metadata:
  name: expand-deployments
spec:
  applyTo:
    - groups: ["apps"]
      kinds: ["DaemonSet", "Deployment", "ReplicaSet", "StatefulSet"]
      versions: ["v1"]
    - groups: [""]
      kinds: ["ReplicationController"]
      versions: ["v1"]
  templateSource: "spec.template"
  generatedGVK:
    kind: "Pod"
    group: ""
    version: "v1"
```

Et re deployer le déploiement. Observer.