# 009-setup-gatekeeper

## Helm

Télécharger et installer helm : 

`curl https://raw.githubusercontent.com/helm/helm/main/scripts/get-helm-3 | bash`

## Gatekeeper

Ajouter le repository helm de gatekeeper et installer la chart : 

```
helm repo add gatekeeper https://open-policy-agent.github.io/gatekeeper/charts
helm install gatekeeper/gatekeeper --name-template=gatekeeper --namespace gatekeeper-system --create-namespace
```

Vérifiez que tout est up : 

`kubectl get pods -n gatekeeper-system`

```
NAME                                             READY   STATUS    RESTARTS   AGE
gatekeeper-audit-557fc5d8d6-52fgs                1/1     Running   0          81s
gatekeeper-controller-manager-65c75c7957-6hwn8   1/1     Running   0          81s
gatekeeper-controller-manager-65c75c7957-8qnx5   1/1     Running   0          81s
gatekeeper-controller-manager-65c75c7957-zl798   1/1     Running   0          81s
```

## Gator-cli

Télécharger le CLI de gatekeeper `gator` : 

```
curl -L https://github.com/open-policy-agent/gatekeeper/releases/download/v3.15.0/gator-v3.15.0-linux-amd64.tar.gz -o gator-v3.15.0-linux-amd64.tar.gz
tar xf gator-v3.15.0-linux-amd64.tar.gz
sudo mv gator /usr/local/bin
rm gator-v3.15.0-linux-amd64.tar.gz
```