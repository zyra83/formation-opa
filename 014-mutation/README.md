# 014-mutation

Supprimer le namespace `foobar` : 

`kubectl delete ns foobar`

Créer un `AssignMetadata` qui appliquer le label `owner: admin` sur les tous les namespaces qui commencent par foo : 

```
apiVersion: mutations.gatekeeper.sh/v1
kind: AssignMetadata
metadata:
  name: annotation-owner
spec:
  match:
    kinds:
    - apiGroups: <TODO>
      kinds: <TODO>
    namespaces: <TODO>
  location: <TODO: metadata.....>
  parameters:
    assign:
      value: <TODO>
```

re-Créer le namespace `foobar` et analyser le.


Créer un `Assign` qui empêche d'utiliser des containers privilégiés dans le namespace `foobar` : 

```
apiVersion: mutations.gatekeeper.sh/v1
kind: Assign
metadata:
  name: prevent-privileged-containers
spec:
  applyTo:
  - groups: <TODO>
    kinds: <TODO>
    versions: <TODO>
  match:
    scope: Namespaced
    kinds:
    - apiGroups: <TODO>
      kinds: <TODO>
    namespaces: <TODO>
  location: <TODO: "spec.containers[name:*].......">
  parameters:
    assign:
      value: <TODO>
```

Valider le `Assign` avec le deploiement situé dans le fichier `httpbin.yaml` : 

`kubectl apply -f httpbin.yaml`.

Une fois cette partie validée, supprimer le déploiement :

`kubectl delete deployment httpbin`

Créer une `AssignImage` pour modifier l'image définie dans le fichier `httpbin.yaml` :

```
apiVersion: mutations.gatekeeper.sh/v1alpha1
kind: AssignImage
metadata:
  name: assign-container-image
spec:
  applyTo:
  - groups: [ "" ]
    kinds: [ "Pod" ]
    versions: [ "v1" ]
  location: "spec.containers[name:httpbin].image"
  parameters:
    assignDomain: "my-local-registry.internal-domain"
    assignPath: "repo/app"
    assignTag: "@sha256:abcde67890123456789abc345678901a"
  match:
    source: "All"
    scope: Namespaced
    namespaces: ["foobar"]
    kinds:
    - apiGroups: [ "*" ]
      kinds: [ "Pod" ]
```

Re-déployer httpbin et analyser la différence !

Cleanup:

```
kubectl delete deployment httpbin
kubectl delete assignimage assign-container-image
kubectl delete assign prevent-privileged-containers
kubectl delete assignmetadata annotation-owner
kubectl delete ns foobar
```