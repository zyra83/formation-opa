# 013-namespace-exemption

cleanup du tp précédent : 

```
for i in $(kubectl get constraint -ojsonpath='{.items[*].metadata.name}') ; do kubectl delete constraint $i ; done
sleep 5
for i in $(kubectl get constrainttemplate -ojsonpath='{.items[*].metadata.name}') ; do kubectl delete constrainttemplate $i ; done
```

Ré-appliquer les premières constraintes : 

```
kubectl apply -f k8srequiredlabels_template.yaml
kubectl apply -f all_ns_must_have_gatekeeper.yaml
```

Compter le nombre de violations : ❯ `kubectl describe k8srequiredlabels.constraints.gatekeeper.sh | grep Total`

Créer l'objet Config dans le namespace `gatekeeper-system` : 

Excluer tous les processes sur le namespace `gatekeeper-system` et tous les namespaces commençant par `kube`

```
apiVersion: config.gatekeeper.sh/v1alpha1
kind: Config
metadata:
  name: config
  namespace: "gatekeeper-system"
spec:
  match:
    - excludedNamespaces: ["gatekeeper-system", "kube*"]
      processes: ["*"]
    - excludedNamespaces: ["foobar"]
      processes: ["webhook", "sync", "mutation-webhook"]

```

Compter de nouveau le nombre de violations sur la constraint : a-t-elle évoluée ? OUI 7 -> 3

Supprimer le namespace `foobar` si il existe sur votre cluster.

Tenter de le re-créer avec la commande `kubectl create namespace foobar` : c'est un échec à cause de la constraint.

Parmi les mots clé ci-dessous, déterminer le meilleur process pour pouvoir créer le namespace `foobar`, mais qu'il soit toujours audité : 

- audit
- webhook
- sync
- mutation-webhook

Tenter de créer le namespace une fois l'objet `config` mis à jour.