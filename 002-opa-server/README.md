# OPA as a server

Il est possible de lancer OPA en mode serveur et de communiquer avec lui avec des requêtes HTTP :

`opa run --server ./policy.rego`

Par défaut, `data.system.main` est utilisé pour répondre aux requêtes de policies sans chemin.
Lorsque des requêtes sont executées sans fournir de chemin, il n'y a pas besoin d'encapsuler l'entrée. Si la décision `data.system.main` n'est pas définie, elle est traitée comme une erreur :

`curl localhost:8181 -i -d @input.json -H 'Content-Type: application/json'`

Il est possible d'utiliser n'importe quelle décision par défault : 

`opa run --server ./policy.rego --set=default_decision=example/allow`

`curl localhost:8181 -i -d @input.json -H 'Content-Type: application/json'`