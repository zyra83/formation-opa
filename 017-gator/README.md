# 017-gator

Tester le fichier `nginx.yaml` vis à vis du dossier `template-and-constraints` :

`gator test -f nginx.yaml -f template-and-constraints`

Corriger `nginx.yaml`

étendre le fichier `deployment.yaml` avec l'expansion template `expansion.yaml` : 

`gator expand --filename deployment.yaml --filename expansion.yaml`

Regarder à présent le dossier `suite`

Le contenu du fichier suite.yaml définit un ensemble de tests unitaires qui peuvent être joués par la commande `gator verify suite.yaml`

Prendre un temps d'analyse sur l'ensemble des fichiers du dossier `suite` et tenter de résoudre la suite de tests.