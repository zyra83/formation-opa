# 018-observability

Installer prometheus et grafana

```
helm repo add prometheus-community https://prometheus-community.github.io/helm-charts
helm install prometheus prometheus-community/kube-prometheus-stack -n prometheus --create-namespace
```

Attendre que tous les pods soient déployés.

Créer un service gatekeeper-metrics : 

```
apiVersion: v1
kind: Service
metadata:
    prometheus.io/path: metrics
    prometheus.io/port: "8888"
    prometheus.io/scrape: "true"
  labels:
    prometheus.io/scrape: "true"
  name: gatekeeper-metrics
  namespace: gatekeeper-system
spec:
  ports:
  - name: http
    port: 8888
    protocol: TCP
    targetPort: 8888
  selector:
    app: gatekeeper
    control-plane: controller-manager
  type: ClusterIP
```

et créer un ServiceMonitor : 

```
apiVersion: monitoring.coreos.com/v1
kind: ServiceMonitor
metadata:
  name: prometheus-gatekeeper
  namespace: prometheus
spec:
  endpoints:
  - honorLabels: true
    path: /metrics
    port: http
    scheme: http
    scrapeTimeout: 30s
  jobLabel: prometheus
  namespaceSelector:
    matchNames:
    - gatekeeper-system
  selector:
    matchLabels:
      prometheus.io/scrape: "true"
```

Patienter un peu le temps que les métriques remontent

 Ouvrir grafana :

`kubectl port-forward svc/prometheus-grafana 8080:80 -n prometheus`

Credentials: admin:prom-operator

Dans la section Explore de grafana, prenez un temps pour observer les différentes métriques. Tenter de construire un dashboard avec les métriques gatekeeper !