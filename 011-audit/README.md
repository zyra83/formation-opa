# 011-audit

Créer la constraint template et la constraint du précédent TP, les ressources sont également disponibles dans ce dossier : 

```
kubectl create namespace violate-constraint
kubectl apply -f k8srequiredlabels_template.yaml
kubectl apply -f all_ns_must_have_gatekeeper.yaml
```

Analyser le status de la constraint :

`kubectl describe constraint ns-must-have-gk`

Mettez à jour la configuration de l'audit controller pour qu'il puisse emettre les evenements d'audit : 

`helm upgrade gatekeeper gatekeeper/gatekeeper -n gatekeeper-system -n gatekeeper-system --set emitAuditEvents=true`

Explorer les logs de l'audit controller : 

`kubectl logs -n gatekeeper-system -l app=gatekeeper,control-plane=audit-controller`

## pubsub

Il s'agit maintenant de configurer l'audit controller pour qu'il emette des évenements sur un redis

Créer la configmap `audit-connection` suivante dans le namespace `gatekeeper-system` :

```yaml
apiVersion: v1
kind: ConfigMap
metadata:
  name: <TODO>
  namespace: <TODO>
data:
  provider: "dapr"
  config: |
    {
      "component": "pubsub"
    }
```

DAPR est une couche d'abstraction vers différents messages brokers. Il est aujourd'hui le seul composant supporté par gatekeeper pour le pubsub des audits. Il sera utilisé commme «passe-plat» vers redis.

```
kubectl create -f dapr_namespace.yaml
helm repo add dapr https://dapr.github.io/helm-charts/
helm upgrade --install dapr dapr/dapr \
--version=1.13 \
--namespace dapr-system \
--set dapr_sidecar_injector.sidecarDropALLCapabilities=true
--wait
```

Installer redis : 

```
kubectl apply -f redis_namespace.yaml
helm repo add bitnami https://charts.bitnami.com/bitnami
helm install redis bitnami/redis --set image.tag=6.2 -n redis
```

Créer le namespace gatekeeper-subscriber et copier les credentials du redis : 

```
kubectl apply -f gatekeeper-subscriber_namespace.yaml
kubectl get secret redis --namespace=redis -o yaml | sed 's/namespace: .*/namespace: gatekeeper-subscriber/' | kubectl apply -f -
```

Ajouter le composant dapr :

`kubectl apply -f redis_component.yaml`

Deployer le consommateur redis : 

`kubectl apply -f fake-subscriber.yaml`

Copier le secret redis dans le namespace `gatekeeper-system` : 

`kubectl get secret redis --namespace=redis -o yaml | sed 's/namespace: .*/namespace: <TODO>/' | kubectl apply -f -`

Activer dapr sur le pod audit-controller : 

```
echo 'auditPodAnnotations: {dapr.io/enabled: "true", dapr.io/app-id: "audit", dapr.io/metrics-port: "9999", dapr.io/sidecar-seccomp-profile-type: "RuntimeDefault"}' > /tmp/annotations.yaml
helm upgrade --install gatekeeper gatekeeper/gatekeeper --namespace gatekeeper-system \
--set audit.enablePubsub=true \
--set audit.connection=audit-connection \
--set audit.channel=audit-channel \
--values /tmp/annotations.yaml
```

Si tout se passe bien, des logs devraient apparaitre dans le fake-subscriber ! 

`kubectl logs -f -l  app=sub -c go-sub -n gatekeeper-subscriber`

## cleanup

```
for i in gatekeeper-subscriber gatekeeper-system ; do kubectl delete component pubsub -n $i ; done
helm uninstall dapr -n dapr-system
kubectl delete namespace dapr-system
helm uninstall redis -n redis
kubectl delete namespace redis
for i in gatekeeper-subscriber gatekeeper-system ; do kubectl delete secret redis -n $i ; done
kubectl delete deployment sub -n gatekeeper-subscriber
kubectl delete ns gatekeeper-subscriber
helm upgrade gatekeeper gatekeeper/gatekeeper -n gatekeeper-system -n gatekeeper-system --set emitAuditEvents=true
```