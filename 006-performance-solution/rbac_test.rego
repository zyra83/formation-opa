package rbac

import rego.v1

test_user_has_role_dev if {
	user_has_role.dev with inp as {"subject": "alice"}
}

test_user_has_role_negative if {
	not user_has_role["super-admin"] with input as {"subject": "alice"}
}