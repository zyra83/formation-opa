`opa eval --data rbac.rego --profile --format=pretty 'data.rbac.allow'`

`opa eval --data rbac.rego --profile --format=pretty --count=10 'data.rbac.allow'`

`opa eval --data rbac.rego --profile-limit 5 --format=pretty 'data.rbac.allow'`

`opa eval --data rbac.rego --profile-limit 5 --format=pretty 'data.rbac.allow' --profile-sort time`

`opa test -v ./rbac.rego ./rbac_test.rego`

`opa test -v ./rbac.rego ./rbac_test.rego --bench`