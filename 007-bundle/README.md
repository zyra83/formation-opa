# 007-bundle

dans un dossier de votre choix, créez l'arboresence suivante :

```
.
├── role
├── test
│   └── server
└── users
```

Créer le fichier `test/server/policy.rego` : 

```
package test.server.rest

default allowed = false

allowed {
    name := input.name
    data.users.users[name].role == "manager"
}
```

Créer le fichier `users/data.json` : 

```
{
    "users" : {
        "bob": {
            "role":"manager"
        },
        "alice": {
            "role":"staff"
        }
    }
}
```

Créer le fichier `role/data.json` : 

```
{
    "role" : ["manager", "staff"]
}
```

Construisez votre bundle à l'aide de la commande `opa build --revision=1.0.0 .`

Le fichier bundle.tar.gz est créé. Analyser son contenu à l'aide de la commande `tar zvtf`

Charger ensuite le bundle avec la commande `opa run --bundle` et tenter de dumper `data`

Il est également possible de signer un bundle et de vérifier si un bundle est signé à son chargement.

Génerer une paire de clés : 

```
openssl genrsa -out private-key.pem 2048
openssl rsa -pubout -in private-key.pem -out public-key.pem
```

Signer le bundle : `opa sign -b --signing-key private-key.pem`

Regarder le fichier `.signatures.json` généré.

Reconstruire le bundle avec les clés : 
`opa build -b --revision=1.0.0 --signing-key private-key.pem --verification-key public-key.pem .`

Charger ensuite le bundle en vérifiant sa clé : 

`opa run -b bundle.tar.gz --verification-key public-key.pem`