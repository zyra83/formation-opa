# 006 performance

évaluer le fichier profiling.rego : 

`opa eval --data profiling.rego --profile 'data.test.p' --format=pretty`

Trouvez l'expression la plus consommatrice

Analyser le fichier `rbac.rego`

Lancer un profiling sur le fichier en évaluant `data.rbac.allow`

Il est possible de profiler la policy plusieurs fois avec l'option `--count`.

Profilez la policy 10 fois et observez la sortie

Il est également possible de ne garder qu'un certain nombre de profiles en sortie avec l'option `--profile-limit` pour n'observer que les lignes les plus consommatrices.

Afficher les 5 premières lignes consommatrices.

Par défault, les lignes sont triées par la colonne num_eval. Il est possible de trier par d'autres colonnes à l'aide de l'option `--profile-sort`.

Afficher les 5 premières lignes triées par la colonne time.

La commande `opa bench` permet d'effectuer des benchmarks sur des policies rego. Bencher la policy rbac avec la commande `opa bench --data rbac.rego 'data.rbac.allow'`

Il est également possible de benchmarker les tests !

Créer un fichier de test rbac_test.rego :

```
package rbac

import rego.v1

test_user_has_role_dev if {
	user_has_role.dev with inp as {"subject": "alice"}
}

test_user_has_role_negative if {
	not user_has_role["super-admin"] with input as {"subject": "alice"}
}
```

Effectuer les tests. Vous pouvez ensuite passer l'option `--bench` à la commande pour benchmarker les tests !