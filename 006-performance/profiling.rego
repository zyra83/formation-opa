package test
import rego.v1

p if {
	a := 1
	b := 2
	c := 3
	x = a + (b * c)
}