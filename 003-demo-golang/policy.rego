package rego

import rego.v1

allow if {                                          # allow égal à true si
    count(violation) == 0                           # il n'y a pas de violation
}

violation contains server.id if {                   # une violation contient un server id si un serveur public contient http dans son protocole
    some server in public_servers
    "http" in server.protocols
}

violation contains server.id if {                   # # une violation contient un server id si un serveur a telnet dans son protocole
    some server in input.servers
    "telnet" in server.protocols
}

public_servers contains server if {                 # définition de la regle public_servers utilisée plus haut
    some server in input.servers

    some port in server.ports
    some input_port in input.ports
    port == input_port.id

    some input_network in input.networks
    input_port.network == input_network.id
    input_network.public
}