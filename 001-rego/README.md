# Setup

Télécharger le CLI OPA : 

```
curl -L -o opa https://openpolicyagent.org/downloads/v0.62.1/opa_linux_amd64_static
chmod +x ./opa
mv opa /usr/local/bin/
```

# Découverte du rego

Lancer le CLI en mode interactif en sourçant le fichier input.json : 

`opa run repl.input:input.json`

Afficher les serveurs en renseignant `input.servers` : 

```
> input.servers
[
  {
    "id": "app",
    "ports": [
      "p1",
      "p2",
      "p3"
    ],
    "protocols": [
      "https",
      "ssh"
    ]
  },
  {
    "id": "db",
    "ports": [
      "p3"
    ],
    "protocols": [
      "mysql"
    ]
  },
  {
    "id": "cache",
    "ports": [
      "p3"
    ],
    "protocols": [
      "memcache"
    ]
  },
  {
    "id": "ci",
    "ports": [
      "p1",
      "p2"
    ],
    "protocols": [
      "http"
    ]
  },
  {
    "id": "busybox",
    "ports": [
      "p1"
    ],
    "protocols": [
      "telnet"
    ]
  }
]
```

## Accéder à un élément d'un tableau

Pour accéder à un élément de tableau, on peut utiliser la syntax «square-bracket» : 

`input.servers[0].protocols[0]` : `https`

Quand on essaie de réferencer une valeur qui n'existe pas, OPA renvoie `undefined` ; essayer avec `input.dontexist`

## Expressions

OPA met à disposition un certain nombres d'expressions et de fonctions built-in, essayez les : 

L'opérateur `==` : 

`input.servers[0].id == "app"`

La fonction `count` :

`count(input.servers[0].protocols) >= 1`

L'opérateur `AND` est représenté par `;`

`input.servers[0].id == "app"; input.servers[0].protocols[0] == "https"`

Si une des expressions est fausse (ou undefined), le résultat est undefined. Essayez en changeant un élément dans la requête.

## Rules

Les règles permettent d'encapsuler de la logique -- on peut les voir comme des fonctions.

Chaque règle peut être soit complète (`complete`), soit partielle (`partial`)

Une règle complete est une règle «if-then» qui assigne une valeur à une variable, par exemple : 

```
any_public_networks := true if {
    some net in input.networks # some network exists and..
    net.public                 # it is public.
}
```

Les valeurs générées par des règles peuvent être requeteées par la variable globale `data` : 

`opa eval --input 001-rego/input.json -d 001-rego/any-pub-net.rego 'data.rego.any_public_networks'`

Il est également possible d'omettre le `:= true` lors de la définition d'une règle : 

```
any_public_networks if {
    some net in input.networks # some network exists and..
    net.public                 # it is public.
}
```

Evaluer cette règle.

Si OPA n'est pas en mesure de satisfaire la règle, celle-ci sera `undefined` : 

`opa eval --input 001-rego/no-pub-net.json -d 001-rego/any-pub-net.rego 'data.rego.any_public_networks'`

Les règles partielles sont des règles qui génèrent un ensemble de valeurs et qui les assignent à une variable : 

```
public_network contains net.id if {
    some net in input.networks # some network exists and..
    net.public                 # it is public.
}
```

Evaluer cette règle avec `input.json` et `no-pub-net.json` : analyser la différence

## Variables

Il est possible de stocker des valeurs dans des variables intermediaires en utilisant l'opérateur d'assignement `:=` : 

```
s := input.servers[0]
s.id == "app"
p := s.protocols[0]
p == "https"

p

s
```

Les variables sont **immutables** : OPA emettra une erreur si une variable est assignée une 2ème fois.

Cette fois ci, déclarer le fichier suivant (`immutable.rego`) et demander à opa d'évaluer l'expression : 

```
package example

import rego.v1

result if {
    s := input.servers[0]
    s := input.servers[1]
}
```

`opa eval --input input.json -d immutable.rego 'data.example.result'`

```
{
  "errors": [
    {
      "message": "var s assigned above",
      "code": "rego_compile_error",
      "location": {
        "file": "test.rego",
        "row": 7,
        "col": 5
      }
    }
  ]
}
```


Si une variable n'est pas déclarée, OPA emettra également un message d'erreur : 

```
package example

import rego.v1

result if {
  x := 1
  x != y
}
```

## Itérations

Pour iterer autour d'un tableau, il est possible d'utiliser le mot clé `some` en substituant l'index du tableau par une variable : 

`some i; input.networks[i].public == true`

Il est possible de définir autant d'itérations que l'on veut : 

`some i, j; input.servers[i].protocols[j] == "http"`

la nouvelle façon d'iterer est d'utiliser l'expression `some ... in ...` : 

```
public_network contains net.id if {
    some net in input.networks # some network exists and..
    net.public                 # it is public.
}

shell_accessible contains server.id if {
    some server in input.servers
    "telnet" in server.protocols
}

shell_accessible contains server.id if {
    some server in input.servers
    "ssh" in server.protocols
}
```

Encapsuler ces règles dans un module rego et évaluer les !

Le mot clé `every` peut également être utilisé pour exprimer succinctement qu'une condition est valable pour tous les éléments d'un domaine : 

```
no_telnet_exposed if {
    every server in input.servers {
        every protocol in server.protocols {
            "telnet" != protocol
        }
    }
}
any_telnet_exposed if {
    some server in input.servers
    "telnet" in server.protocols
}
```

Encapsuler ces règles dans un module rego et évaluer les !

Pourquoi no_telnet_exposed n'apparait pas ? Tenter une copie de input.json et modifier le

## OU (OR)

Lorsque plusieurs expressions sont jointes ensemble dans une même requête, c'est la logique ET (AND) qui est appliquée.

Pour utiliser la logique OU (OR), il suffit de définir plusieurs règles avec le même nom :

```
package rego

import rego.v1

default shell_accessible := false

shell_accessible if {
	input.servers[_].protocols[_] == "telnet"
}

shell_accessible if {
	input.servers[_].protocols[_] == "ssh"
}
```

Evaluer cette règle !

Il est également possible d'utiliser la notion de «logical or» sur des règles partielles : 

```
package rego

import rego.v1

shell_accessible contains server.id if {
	server := input.servers[_]
	server.protocols[_] == "telnet"
}

shell_accessible contains server.id if {
	server := input.servers[_]
	server.protocols[_] == "ssh"
}
```

Evaluer cette règle !

## Première implémentation fonctionnelle d'une policy rego 

1) Les serveurs joignables depuis internet ne doivent pas exposer le protocole HTTP
2) Les serveurs ne sont pas autorisés à exposer le protocole telnet

Compléter la policy suivante et évaluer la : 

```
package rego

import rego.v1

allow if {                                          # allow égal à true si
    count(violation) == <XX>                           # il n'y a pas de violation
}

violation contains server.id if {                   # une violation contient un server id si un serveur public contient http dans son protocole
    some server in public_servers
    <TODO> in server.protocols
}

violation contains server.id if {                   # # une violation contient un server id si un serveur a telnet dans son protocole
    some server in input.servers
    <TODO> in server.protocols
}

public_servers contains server if {                 # définition de la regle public_servers utilisée plus haut
    some server in input.servers

    some port in server.ports
    some input_port in input.ports
    port == input_port.id

    some input_network in input.networks
    input_port.network == input_network.id
    input_network.public
}
```

Hint: pour lister les violations, on peut utiliser l'opérateur `some` sur `data.rego.violation` : 

`opa eval --input 001-rego/input.json -d policy.rego 'OPA expression'`