package rego

import rego.v1


any_public_networks  if {
    some net in input.networks # some network exists and..
    net.public                 # it is public.
}