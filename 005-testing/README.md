# 005-testing


Ci dessous une policy rego qui autorise l'accès si le role de l'utilisateur est admin : 

```
package example

allow_access {
    input.user.role == "admin"
}
```

Testez la règle avec différentes entrées pour vous assurer qu'elle fonctionne comme prévu de façon manuelle

Écrivez des tests unitaires pour la règle Rego créée précédemment.
Exécutez les tests unitaires pour vous assurer que la règle fonctionne correctement.


Afficher les résultats des tests

Afficher également le coverage des tests réalisés