package example

# appel des tests avec
# ❯ opa test . -v
# ❯ opa test . -v

allow_access {
    input.user.role == "admin"
}
