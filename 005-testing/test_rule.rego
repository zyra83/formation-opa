package example

test_allow_access {
    allow_access with input as {"user": {"role": "admin"}}
}

test_disallow_access {
    not allow_access with input as {"user": {"role": "pasadmin"}}
}
