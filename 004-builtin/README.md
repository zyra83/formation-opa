# 004 built in

Voici une série de petits exercices pour prendre en main les différentes fonction builtin de rego.

Vous pouvez, soit lancer le cli en mode interactif en sourçant un fichier rego : `opa run file.rego` et récuperer les valeurs de policies : `data.my_package.my_policy` ou lancer une évaluation à chaque fois : `opa eval -d file.rego "data.my_package.my_policy"`

## les nombres

- Définissez la variable my_number à `1.25` et afficher sa valeur arrondie à l'entier supérieur
  > floor
- Passer la variable my_number à -10 et afficher sa valeur absolue
  > ceilabs
- Définissez dans une variable un tableau allant de 5 à 9
  > import rego.v1
  > arr := numbers.range(5,9)

## les types

- Définissez la variable my_str à "hello, world" et afficher son type
  > type_name(my_str)
- vérifier que cette variable est bien une string
  >  type_name(my_str) == "string"
- vérifier que cette variable n'est pas un nombre
  > type_name(my_str) != "number"
- vérifier que cette variable n'est pas nulle
  > is_null(my_str) == false
- Définissez my_number à "123" et vérifier que c'est une string
  > my_number = "123"
  > type_name(my_number) == "string"
- Transformer cette variable en nombre
  > to_number(my_number)
  > my_numnum := to_number(my_number)

## les agrégations

- Définissez le set my_set avec pour valeur [10,6,9,54,6,7]
  > my_set := [10,6,9,54,6,7]
- Afficher le nombre d'élements de ce set
  > count(myset)
- Afficher le mini et le max de ce set
   > min max
- Afficher la somme de tous ces éléments
  > sum(my_set)

## les valeurs composites

- Définissez arr1 et arr2 avec pour valeur respectives ["hello", "world"] et ["goodbye", "everyone"]
  ```rego
  arr1 := ["hello", "world"]
  arr2 := ["goodbye", "everyone"]

  ```
  
- Fusionner les 2 tableaux dans une variable
  > arr_concat = array.concat(arr1,arr2)
- N'afficher que les 2 derniers éléments de ce nouveau tableau
  > array.slice(arr_concat, count(arr_concat) - 2, count(arr_concat))

- Créer un objet composite avec les valeurs suivantes : 
```
{
  "user": {
    "id": 1,
    "username": "john_doe",
    "first_name": "John",
    "last_name": "Doe",
    "email": "john.doe@example.com",
    "role": "user",
    "active": true,
    "preferences": {
      "theme": "dark",
      "language": "en"
    }
  }
}
```
- filtrer uniquement sur la langue préférée de l'utilisateur
- supprimer son champ email

## les string

- écrire une règle OPA qui autorise l'accès uniquement si le nom complet de l'utilisateur est "John Doe".
- écrire une règle OPA qui autorise l'accès uniquement si le nom d'utilisateur a une longueur d'au moins 5 caractères.
- écrire une règle OPA qui autorise l'accès uniquement si l'e-mail de l'utilisateur commence par "admin@".
- écrire une règle OPA qui autorise l'accès uniquement si le mot "guest" n'est pas présent dans le champ "role" de l'utilisateur.
- écrire une règle OPA qui autorise l'accès uniquement si le rôle de l'utilisateur est "ADMIN" (en majuscules).
- écrire une règle OPA qui autorise l'accès uniquement si les deux premiers caractères du champ "username" de l'utilisateur sont "JD".
- écrire une règle OPA qui autorise l'accès uniquement si le nom d'utilisateur ne contient pas le mot "blocked". Si c'est le cas, remplacer-le par "user".

## les regexp

- écrire une règle OPA qui autorise l'accès uniquement si l'e-mail de l'utilisateur est au format correct (user@example.com).
- écrire une règle OPA qui autorise l'accès uniquement si le mot de passe de l'utilisateur respecte les critères suivants : au moins 8 caractères, au moins une lettre majuscule, une lettre minuscule et un chiffre.
- écrire une règle OPA qui autorise l'accès uniquement si l'URL de la ressource contient "api" dans son chemin.
- écrire une règle OPA qui autorise l'accès uniquement si le nom du fichier de la ressource se termine par l'extension ".txt".
- écrire une règle OPA qui autorise l'accès uniquement si le numéro de téléphone de l'utilisateur est au format valide (par exemple, "+123456789").
- écrire une règle OPA qui autorise l'accès uniquement si l'identifiant de l'utilisateur est composé uniquement de lettres minuscules et de chiffres.

## jwt

- définissez un jwt avec pour valeur `eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.7iMCOyT_C2dXdorMdC-8mYDRjXpRBQcIh9P8ldzK8iU`
- décoder le jwt et vérifier qu'il est valide