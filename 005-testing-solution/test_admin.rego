package example

test_allow_access {
    allow_access with input as {"user": {"role": "admin"}}
}

test_deny_access {
    not allow_access with input as {"user": {"role": "user"}}
}

test_deny_access_no_role {
    not allow_access with input as {"user": {}}
}