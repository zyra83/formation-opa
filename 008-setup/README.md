# 008-setup

Installer kind :

```bash
# For AMD64 / x86_64
[ $(uname -m) = x86_64 ] && curl -Lo ./kind https://kind.sigs.k8s.io/dl/v0.20.0/kind-linux-amd64
# For ARM64
[ $(uname -m) = aarch64 ] && curl -Lo ./kind https://kind.sigs.k8s.io/dl/v0.20.0/kind-linux-arm64
chmod +x ./kind
sudo mv ./kind /usr/local/bin/kind
```

Créer un cluster kubernetes :

```bash
kind create cluster
```

Vérifier que le cluster est up & running :

```bash
kubectl cluster-info --context kind-kind
```

Note: au fur et à mesure des TPs, on va se retrouver avec beaucoup de pods, et des erreurs sur les pods liées au "too
many open files" peuvent apparaitre.

Pour y remédier, il convient d'augmenter les limites du système.

De façon temporaire (oubliées au redémarrage) :
```
sudo sysctl fs.inotify.max_user_watches=524288
sudo sysctl fs.inotify.max_user_instances=512
```

De façon permanente, il faut ajouter les lignes suivantes dans /etc/sysctlf.conf

```
fs.inotify.max_user_watches = 524288
fs.inotify.max_user_instances = 512
```

et lancer sysctl :

`sysctl -p /etc/sysctl.conf`

